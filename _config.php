<?php

define('SIMPLE_ICONS_SELECT_MODULE', 'simple-line-icons-select');

if (basename(dirname(__FILE__)) != SIMPLE_ICONS_SELECT_MODULE) {
	throw new Exception(SIMPLE_ICONS_SELECT_MODULE . ' module not installed in correct directory');
}