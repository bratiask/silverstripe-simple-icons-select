<?php

class SimpleIconsSelectField extends TextField
{
    function Field($properties = array()){
        Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
        Requirements::javascript(THIRDPARTY_DIR . '/jquery-entwine/dist/jquery.entwine-dist.js');
        Requirements::javascript(SELECT2_MODULE . "/select2/select2.js");
        Requirements::javascript(SIMPLE_ICONS_SELECT_MODULE . '/javascript/simple-icons-select.js');

        Requirements::css(SELECT2_MODULE . "/select2/select2.css");
        Requirements::css(SIMPLE_ICONS_SELECT_MODULE . "/css/simple-icons-select.css");

        return parent::Field($properties);
    }

    /**
     * @return Array
     */
    public function getAttributes() {
        $attributes = array_merge(
            parent::getAttributes(),
            array(
                'type' => 'hidden'
            )
        );

        return $attributes;
    }
}