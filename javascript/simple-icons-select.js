(function($) {
		$("input.simpleiconsselect").entwine({
            onmatch: function() {
                var self = this;
                self.select2({
                    data: [{
                        id: 'user-female',
                        text: 'user-female'
                    },
                        {
                            id: 'user-follow',
                            text: 'user-follow'
                        },
                        {
                            id: 'user-following',
                            text: 'user-following'
                        },
                        {
                            id: 'user-unfollow',
                            text: 'user-unfollow'
                        },
                        {
                            id: 'trophy',
                            text: 'trophy'
                        },
                        {
                            id: 'screen-smartphone',
                            text: 'screen-smartphone'
                        },
                        {
                            id: 'screen-desktop',
                            text: 'screen-desktop'
                        },
                        {
                            id: 'plane',
                            text: 'plane'
                        },
                        {
                            id: 'notebook',
                            text: 'notebook'
                        },
                        {
                            id: 'moustache',
                            text: 'moustache'
                        },
                        {
                            id: 'mouse',
                            text: 'mouse'
                        },
                        {
                            id: 'magnet',
                            text: 'magnet'
                        },
                        {
                            id: 'energy',
                            text: 'energy'
                        },
                        {
                            id: 'emoticon-smile',
                            text: 'emoticon-smile'
                        },
                        {
                            id: 'disc',
                            text: 'disc'
                        },
                        {
                            id: 'cursor-move',
                            text: 'cursor-move'
                        },
                        {
                            id: 'crop',
                            text: 'crop'
                        },
                        {
                            id: 'credit-card',
                            text: 'credit-card'
                        },
                        {
                            id: 'chemistry',
                            text: 'chemistry'
                        },
                        {
                            id: 'user',
                            text: 'user'
                        },
                        {
                            id: 'speedometer',
                            text: 'speedometer'
                        },
                        {
                            id: 'social-youtube',
                            text: 'social-youtube'
                        },
                        {
                            id: 'social-twitter',
                            text: 'social-twitter'
                        },
                        {
                            id: 'social-tumblr',
                            text: 'social-tumblr'
                        },
                        {
                            id: 'social-facebook',
                            text: 'social-facebook'
                        },
                        {
                            id: 'social-dropbox',
                            text: 'social-dropbox'
                        },
                        {
                            id: 'social-dribbble',
                            text: 'social-dribbble'
                        },
                        {
                            id: 'shield',
                            text: 'shield'
                        },
                        {
                            id: 'screen-tablet',
                            text: 'screen-tablet'
                        },
                        {
                            id: 'magic-wand',
                            text: 'magic-wand'
                        },
                        {
                            id: 'hourglass',
                            text: 'hourglass'
                        },
                        {
                            id: 'graduation',
                            text: 'graduation'
                        },
                        {
                            id: 'ghost',
                            text: 'ghost'
                        },
                        {
                            id: 'game-controller',
                            text: 'game-controller'
                        },
                        {
                            id: 'fire',
                            text: 'fire'
                        },
                        {
                            id: 'eyeglasses',
                            text: 'eyeglasses'
                        },
                        {
                            id: 'envelope-open',
                            text: 'envelope-open'
                        },
                        {
                            id: 'envelope-letter',
                            text: 'envelope-letter'
                        },
                        {
                            id: 'bell',
                            text: 'bell'
                        },
                        {
                            id: 'badge',
                            text: 'badge'
                        },
                        {
                            id: 'anchor',
                            text: 'anchor'
                        },
                        {
                            id: 'wallet',
                            text: 'wallet'
                        },
                        {
                            id: 'vector',
                            text: 'vector'
                        },
                        {
                            id: 'speech',
                            text: 'speech'
                        },
                        {
                            id: 'puzzle',
                            text: 'puzzle'
                        },
                        {
                            id: 'printer',
                            text: 'printer'
                        },
                        {
                            id: 'present',
                            text: 'present'
                        },
                        {
                            id: 'playlist',
                            text: 'playlist'
                        },
                        {
                            id: 'pin',
                            text: 'pin'
                        },
                        {
                            id: 'picture',
                            text: 'picture'
                        },
                        {
                            id: 'map',
                            text: 'map'
                        },
                        {
                            id: 'layers',
                            text: 'layers'
                        },
                        {
                            id: 'handbag',
                            text: 'handbag'
                        },
                        {
                            id: 'globe-alt',
                            text: 'globe-alt'
                        },
                        {
                            id: 'globe',
                            text: 'globe'
                        },
                        {
                            id: 'frame',
                            text: 'frame'
                        },
                        {
                            id: 'folder-alt',
                            text: 'folder-alt'
                        },
                        {
                            id: 'film',
                            text: 'film'
                        },
                        {
                            id: 'feed',
                            text: 'feed'
                        },
                        {
                            id: 'earphones-alt',
                            text: 'earphones-alt'
                        },
                        {
                            id: 'earphones',
                            text: 'earphones'
                        },
                        {
                            id: 'drop',
                            text: 'drop'
                        },
                        {
                            id: 'drawer',
                            text: 'drawer'
                        },
                        {
                            id: 'docs',
                            text: 'docs'
                        },
                        {
                            id: 'directions',
                            text: 'directions'
                        },
                        {
                            id: 'direction',
                            text: 'direction'
                        },
                        {
                            id: 'diamond',
                            text: 'diamond'
                        },
                        {
                            id: 'cup',
                            text: 'cup'
                        },
                        {
                            id: 'compass',
                            text: 'compass'
                        },
                        {
                            id: 'call-out',
                            text: 'call-out'
                        },
                        {
                            id: 'call-in',
                            text: 'call-in'
                        },
                        {
                            id: 'call-end',
                            text: 'call-end'
                        },
                        {
                            id: 'calculator',
                            text: 'calculator'
                        },
                        {
                            id: 'bubbles',
                            text: 'bubbles'
                        },
                        {
                            id: 'briefcase',
                            text: 'briefcase'
                        },
                        {
                            id: 'book-open',
                            text: 'book-open'
                        },
                        {
                            id: 'basket-loaded',
                            text: 'basket-loaded'
                        },
                        {
                            id: 'basket',
                            text: 'basket'
                        },
                        {
                            id: 'bag',
                            text: 'bag'
                        },
                        {
                            id: 'action-undo',
                            text: 'action-undo'
                        },
                        {
                            id: 'action-redo',
                            text: 'action-redo'
                        },
                        {
                            id: 'wrench',
                            text: 'wrench'
                        },
                        {
                            id: 'umbrella',
                            text: 'umbrella'
                        },
                        {
                            id: 'trash',
                            text: 'trash'
                        },
                        {
                            id: 'tag',
                            text: 'tag'
                        },
                        {
                            id: 'support',
                            text: 'support'
                        },
                        {
                            id: 'size-fullscreen',
                            text: 'size-fullscreen'
                        },
                        {
                            id: 'size-actual',
                            text: 'size-actual'
                        },
                        {
                            id: 'shuffle',
                            text: 'shuffle'
                        },
                        {
                            id: 'share-alt',
                            text: 'share-alt'
                        },
                        {
                            id: 'share',
                            text: 'share'
                        },
                        {
                            id: 'rocket',
                            text: 'rocket'
                        },
                        {
                            id: 'question',
                            text: 'question'
                        },
                        {
                            id: 'pie-chart',
                            text: 'pie-chart'
                        },
                        {
                            id: 'pencil',
                            text: 'pencil'
                        },
                        {
                            id: 'note',
                            text: 'note'
                        },
                        {
                            id: 'music-tone-alt',
                            text: 'music-tone-alt'
                        },
                        {
                            id: 'music-tone',
                            text: 'music-tone'
                        },
                        {
                            id: 'microphone',
                            text: 'microphone'
                        },
                        {
                            id: 'loop',
                            text: 'loop'
                        },
                        {
                            id: 'logout',
                            text: 'logout'
                        },
                        {
                            id: 'login',
                            text: 'login'
                        },
                        {
                            id: 'list',
                            text: 'list'
                        },
                        {
                            id: 'like',
                            text: 'like'
                        },
                        {
                            id: 'home',
                            text: 'home'
                        },
                        {
                            id: 'grid',
                            text: 'grid'
                        },
                        {
                            id: 'graph',
                            text: 'graph'
                        },
                        {
                            id: 'equalizer',
                            text: 'equalizer'
                        },
                        {
                            id: 'dislike',
                            text: 'dislike'
                        },
                        {
                            id: 'cursor',
                            text: 'cursor'
                        },
                        {
                            id: 'control-start',
                            text: 'control-start'
                        },
                        {
                            id: 'control-rewind',
                            text: 'control-rewind'
                        },
                        {
                            id: 'control-play',
                            text: 'control-play'
                        },
                        {
                            id: 'control-pause',
                            text: 'control-pause'
                        },
                        {
                            id: 'control-forward',
                            text: 'control-forward'
                        },
                        {
                            id: 'control-end',
                            text: 'control-end'
                        },
                        {
                            id: 'calendar',
                            text: 'calendar'
                        },
                        {
                            id: 'bulb',
                            text: 'bulb'
                        },
                        {
                            id: 'bar-chart',
                            text: 'bar-chart'
                        },
                        {
                            id: 'arrow-up',
                            text: 'arrow-up'
                        },
                        {
                            id: 'arrow-right',
                            text: 'arrow-right'
                        },
                        {
                            id: 'arrow-left',
                            text: 'arrow-left'
                        },
                        {
                            id: 'arrow-down',
                            text: 'arrow-down'
                        },
                        {
                            id: 'ban',
                            text: 'ban'
                        },
                        {
                            id: 'bubble',
                            text: 'bubble'
                        },
                        {
                            id: 'camcorder',
                            text: 'camcorder'
                        },
                        {
                            id: 'camera',
                            text: 'camera'
                        },
                        {
                            id: 'check',
                            text: 'check'
                        },
                        {
                            id: 'clock',
                            text: 'clock'
                        },
                        {
                            id: 'close',
                            text: 'close'
                        },
                        {
                            id: 'cloud-download',
                            text: 'cloud-download'
                        },
                        {
                            id: 'cloud-upload',
                            text: 'cloud-upload'
                        },
                        {
                            id: 'doc',
                            text: 'doc'
                        },
                        {
                            id: 'envelope',
                            text: 'envelope'
                        },
                        {
                            id: 'eye',
                            text: 'eye'
                        },
                        {
                            id: 'flag',
                            text: 'flag'
                        },
                        {
                            id: 'folder',
                            text: 'folder'
                        },
                        {
                            id: 'heart',
                            text: 'heart'
                        },
                        {
                            id: 'info',
                            text: 'info'
                        },
                        {
                            id: 'key',
                            text: 'key'
                        },
                        {
                            id: 'link',
                            text: 'link'
                        },
                        {
                            id: 'lock',
                            text: 'lock'
                        },
                        {
                            id: 'lock-open',
                            text: 'lock-open'
                        },
                        {
                            id: 'magnifier',
                            text: 'magnifier'
                        },
                        {
                            id: 'magnifier-add',
                            text: 'magnifier-add'
                        },
                        {
                            id: 'magnifier-remove',
                            text: 'magnifier-remove'
                        },
                        {
                            id: 'paper-clip',
                            text: 'paper-clip'
                        },
                        {
                            id: 'paper-plane',
                            text: 'paper-plane'
                        },
                        {
                            id: 'plus',
                            text: 'plus'
                        },
                        {
                            id: 'pointer',
                            text: 'pointer'
                        },
                        {
                            id: 'power',
                            text: 'power'
                        },
                        {
                            id: 'refresh',
                            text: 'refresh'
                        },
                        {
                            id: 'reload',
                            text: 'reload'
                        },
                        {
                            id: 'settings',
                            text: 'settings'
                        },
                        {
                            id: 'star',
                            text: 'star'
                        },
                        {
                            id: 'symbol-female',
                            text: 'symbol-female'
                        },
                        {
                            id: 'symbol-male',
                            text: 'symbol-male'
                        },
                        {
                            id: 'target',
                            text: 'target'
                        },
                        {
                            id: 'volume-1',
                            text: 'volume-1'
                        },
                        {
                            id: 'volume-2',
                            text: 'volume-2'
                        },
                        {
                            id: 'volume-off',
                            text: 'volume-off'
                        },
                        {
                            id: 'users',
                            text: 'users'
                        }],
                    allowClear: true,
                    placeholder: 'Choose icon',
                    initSelection: function(element, callback) {
                        callback({
                            id: $(element).val(),
                            text: $(element).val()
                        });
                    },
                    formatResult: function (item) {
                        return '<div><span aria-hidden="true" class="icon-' + item.id + '" style="font-size: 28px; vertical-align: middle;"></span>&nbsp;' + item.text + '</div>';
                    },
                    formatSelection: function (item) {
                        return '<i aria-hidden="true" class="icon-' + item.id + '" style="font-size: 16px; vertical-align: middle;"></i>&nbsp;' + item.text + '';
                    },
                    dropdownCssClass: "bigdrop",
                    escapeMarkup: function (m) {
                        return m;
                    }
                });
            }

		});
}(jQuery));